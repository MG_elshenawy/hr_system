<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InsurranceCompany extends Model
{
    use HasFactory;
    protected $guarded = [];
}
